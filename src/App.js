import React from "react";
import ReactDOM from "react-dom";
import { EditorState } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import "./App.css";
function App() {
  const [editorState, setEditorState] = React.useState(() =>
    EditorState.createEmpty()
  );
  const customStyleMap = {
    FONT_SIZE_16: {
      fontSize: "30px",
    },
  };
  return (
    <div style={{ width: "600px", height: "30vh" }}>
      <Editor
        customStyleMap={customStyleMap}
        editorState={editorState}
        // onChange={setEditorState}
        onEditorStateChange={setEditorState}
        wrapperClassName="wrapper-class"
        editorClassName="editor-class"
        toolbarClassName="toolbar-class"
      />
    </div>
  );
}

export default App;
